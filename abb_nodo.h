#ifndef ABB_ABB_NODO_H
#define ABB_ABB_NODO_H

#include "abb.h" // para abb_destruir_dato_t

typedef struct abb_nodo {
    char* _clave;
    void* _valor;
    struct abb_nodo * _padre;
    struct abb_nodo * _hizo_izq;
    struct abb_nodo * _hijo_der;
} abb_nodo_t;

abb_nodo_t * abb_nodo_crear(const char* clave, void* valor, abb_nodo_t * padre, abb_nodo_t * hijo_izq, abb_nodo_t * hijo_der);
void abb_nodo_destruir(abb_nodo_t * abb_nodo, abb_destruir_dato_t destruir_dato);
const char* abb_nodo_obtener_clave(abb_nodo_t* abb_nodo);
void* abb_nodo_obtener_valor(abb_nodo_t* abb_nodo);
void abb_nodo_reemplazar_valor(abb_nodo_t* abb_nodo, void* valor_nuevo, abb_destruir_dato_t destruir_valor);
bool abb_nodo_es_hijo_izquierdo(abb_nodo_t* hijo);
bool abb_nodo_tiene_hijo_izquierdo(abb_nodo_t* nodo);
bool abb_nodo_tiene_hijo_derecho(abb_nodo_t* nodo);

#endif //ABB_ABB_NODO_H
