/*
 * Pruebas para el tipo de dato abstracto Árbol Binario de Búsqueda.
 * TODO: Cŕeditos
 */

#include "abb.h"
#include "testing.h"
#include <stdlib.h>
#include <string.h>
#include "pila.h"
#include <stdio.h>

int abb_comparar_clave_1(const char *clave1, const char *clave2){
    return strcmp(clave1, clave2);
}

char* prueba_copiar_clave(const char* clave){
    char* copia = malloc(1 + strlen(clave));
    if(!copia){
        return NULL;
    }
    strcpy(copia, clave);
    return copia;
}

char* crear_clave_aleatoria(size_t largo) {
    const char caracteres[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJK";
    char* cadena = malloc(largo + 1);
    for (size_t n = 0; n < largo; n++) {
        int key = rand() % (int) (largo);
        cadena[n] = caracteres[key];
    }
    cadena[largo] = '\0';
return cadena;
}

/* ******************************************************************
 *         FUNCIONES AUXILIARES PARA PROBAR INTERADOR INTERNO
 * *****************************************************************/

bool visitar_sumar_valores_pares(const char* clave, void* dato, void* extra){
    int* suma = extra;
    int* valor = dato;

    int resto = *valor % 2;
    if(resto == 0){
        *suma += *valor;
    }
    return true;
}

bool visitar_sumar_valores_menores_a_15(const char* clave, void* dato, void* extra){
    int* suma = extra;
    int* valor = dato;

    if(*valor < 15){
        *suma += *valor;
        return true;
    }
    else{
        return false;
    }
}

/* ******************************************************************
 *            ARBOL AUXILIAR QUE SE UTILIZA EN VARIAS PRUEBAS
 * *****************************************************************/

char* claves[] = {"12", "05", "02", "09", "18", "15", "19", "13", "16", "07", "20", "17"};
char* claves_in_order[] = {"02", "05", "07", "09", "12", "13", "15", "16", "17", "18", "19", "20"};
int v12 = 12, v5 = 5, v2 = 2, v9 = 9, v18 = 18, v15 = 15, v19 = 19, v13 = 13, v16 = 16, v7 = 7, v20 = 20, v17 = 17;
int vbis12 = 120, vbis5 = 50, vbis2 = 20, vbis9 = 90, vbis18 = 180, vbis15 = 150, vbis19 = 190, vbis13 = 130, vbis16 = 160, vbis7 = 70, vbis20 = 200, vbis17 = 170;
int suma_valores_pares = 68;
int suma_valores_menores_a_15 = 48;

int* valores[] = {&v12, &v5, &v2, &v9, &v18, &v15, &v19, &v13, &v16, &v7, &v20, &v17};
int* valores_in_order[] = {&v2, &v5, &v7, &v9, &v12, &v13, &v15, &v16, &v17, &v18, &v19, &v20};
int* valores_bis[] = {&vbis12, &vbis5, &vbis2, &vbis9, &vbis18, &vbis15, &vbis19, &vbis13, &vbis16, &vbis7, &vbis20, &vbis17};
const size_t cant = 12;

abb_t* pruebas_crear_arbol(){

    abb_t* abb = abb_crear(abb_comparar_clave_1, NULL);
    if(!abb) return NULL;

    bool guardar_ok;
    for(size_t i = 0; i < cant; i++){
        guardar_ok = abb_guardar(abb, claves[i], valores[i]);
        if(!guardar_ok) return NULL;
    }

    if(abb_cantidad(abb) != cant) return NULL;
    return abb;
}

/* ******************************************************************
 *                   PRUEBAS UNITARIAS ALUMNO
 * *****************************************************************/

void prueba_crear_abb_vacio()
{
    printf("INICIO PRUEBA ARBOL VACIO\n");
    abb_t* abb = abb_crear(abb_comparar_clave_1, NULL);

    print_test("Prueba abb crear abb vacio", abb);
    print_test("Prueba abb la cantidad de elementos es 0", abb_cantidad(abb) == 0);
    print_test("Prueba abb obtener clave A, es NULL, no existe", !abb_obtener(abb, "A"));
    print_test("Prueba abb pertenece clave A, es false, no existe", !abb_pertenece(abb, "A"));
    print_test("Prueba abb borrar clave A, es NULL, no existe", !abb_borrar(abb, "A"));

    abb_destruir(abb);
    printf("FIN PRUEBA ARBOL VACIO\n");
}

void prueba_abb_clave_vacia()
{
    printf("\n");
    printf("INICIO PRUEBA CLAVE VACIA\n");
    abb_t* abb = abb_crear(abb_comparar_clave_1, NULL);

    char *clave = "", *valor = "";

    print_test("Prueba abb insertar clave vacia", abb_guardar(abb, clave, valor));
    print_test("Prueba abb la cantidad de elementos es 1", abb_cantidad(abb) == 1);
    print_test("Prueba abb obtener clave vacia es valor", abb_obtener(abb, clave) == valor);
    print_test("Prueba abb pertenece clave vacia, es true", abb_pertenece(abb, clave));
    print_test("Prueba abb borrar clave vacia, es valor", abb_borrar(abb, clave) == valor);
    print_test("Prueba abb la cantidad de elementos es 0", abb_cantidad(abb) == 0);

    abb_destruir(abb);
    printf("FIN PRUEBA CLAVE VACIA\n");
}

static void prueba_abb_valor_null()
{
    printf("\n");
    printf("INICIO PRUEBA VALOR NULL\n");
    abb_t* abb = abb_crear(abb_comparar_clave_1, NULL);

    char *clave = "", *valor = NULL;

    /* Inserta 1 valor y luego lo borra */
    print_test("Prueba abb insertar clave vacia valor NULL", abb_guardar(abb, clave, valor));
    print_test("Prueba abb la cantidad de elementos es 1", abb_cantidad(abb) == 1);
    print_test("Prueba abb obtener clave vacia es valor NULL", abb_obtener(abb, clave) == valor);
    print_test("Prueba abb pertenece clave vacia, es true", abb_pertenece(abb, clave));
    print_test("Prueba abb borrar clave vacia, es valor NULL", abb_borrar(abb, clave) == valor);
    print_test("Prueba abb la cantidad de elementos es 0", abb_cantidad(abb) == 0);

    abb_destruir(abb);
    printf("FIN PRUEBA VALOR NULL\n");
}

void prueba_abb_insertar()
{
    printf("\n");
    printf("INICIO PRUEBA INSERTAR\n");
    abb_t* abb = abb_crear(abb_comparar_clave_1, NULL);

    char *clave1 = "1", *valor1 = "v1";
    char *clave2 = "2", *valor2 = "v2";
    char *clave3 = "3", *valor3 = "v3";

    /* Inserta 1 valor y luego lo borra */
    print_test("Prueba abb insertar clave1", abb_guardar(abb, clave1, valor1));
    print_test("Prueba abb la cantidad de elementos es 1", abb_cantidad(abb) == 1);
    print_test("Prueba abb obtener clave1 es valor1", abb_obtener(abb, clave1) == valor1);
    print_test("Prueba abb obtener clave1 es valor1", abb_obtener(abb, clave1) == valor1);
    print_test("Prueba abb pertenece clave1, es true", abb_pertenece(abb, clave1));
    print_test("Prueba abb borrar clave1, es valor1", abb_borrar(abb, clave1) == valor1);
    print_test("Prueba abb la cantidad de elementos es 0", abb_cantidad(abb) == 0);

    /* Inserta otros 2 valores y no los borra (se destruyen con el abb) */
    print_test("Prueba abb insertar clave3", abb_guardar(abb, clave3, valor3));
    print_test("Prueba abb la cantidad de elementos es 1", abb_cantidad(abb) == 1);
    print_test("Prueba abb obtener clave3 es valor3", abb_obtener(abb, clave3) == valor3);
    print_test("Prueba abb obtener clave3 es valor3", abb_obtener(abb, clave3) == valor3);
    print_test("Prueba abb pertenece clave3, es true", abb_pertenece(abb, clave3));

    print_test("Prueba abb insertar clave2", abb_guardar(abb, clave2, valor2));
    print_test("Prueba abb la cantidad de elementos es 2", abb_cantidad(abb) == 2);
    print_test("Prueba abb obtener clave2 es valor2", abb_obtener(abb, clave2) == valor2);
    print_test("Prueba abb obtener clave2 es valor2", abb_obtener(abb, clave2) == valor2);
    print_test("Prueba abb pertenece clave2, es true", abb_pertenece(abb, clave2));

    abb_destruir(abb);
    printf("FIN PRUEBA INSERTAR\n");
}

void prueba_reemplazar_aux(const char* clave, int* valor, int* valor_bis){
    printf("clave: %s, valor: %i, valor_bis: %i\n", clave, *valor, *valor_bis);
    abb_t* abb = pruebas_crear_arbol();
    print_test("Prueba abb crear arbol", abb);
    print_test("  Prueba abb pertenece clave", abb_pertenece(abb, clave));
    print_test("  Prueba abb reemplazar valor con valor_bis", (abb_guardar(abb, clave, valor_bis)));
    print_test("  Prueba abb pertenece clave es true", abb_pertenece(abb, clave));
    print_test("  Prueba abb obtener(clave) == valor_bis", abb_obtener(abb, clave) == valor_bis);

    // Pruebo que los valores que quedaron están bien
    bool otros_valores_ok = true;
    for(size_t i = 0; i < cant ; i++){
        const char* clave_i = claves[i];
        int* valor_i = valores[i];
        if(strcmp(clave_i, clave) != 0){
            if(abb_obtener(abb, clave_i) != valor_i){
                otros_valores_ok = false;
                break;
            }
        }
    }
    print_test("  Prueba abb los valores que quedaron son los correctos", otros_valores_ok);

    abb_destruir(abb);
}

void prueba_abb_reemplazar()
{
    printf("\n");
    printf("INICIO PRUEBA REEMPLAZAR\n");
    abb_t* abb = pruebas_crear_arbol();

    for(size_t i = 0; i < cant; i++){
        prueba_reemplazar_aux(claves[i], valores[i], valores_bis[i]);
    }

    abb_destruir(abb);
    printf("FIN PRUEBA REEMPLAZAR\n");
}

void prueba_abb_reemplazar_con_destruir()
{
    printf("\n");
    printf("INICIO PRUEBA REEMPLAZAR CON DESTRUIR\n");
    abb_t* abb = abb_crear(abb_comparar_clave_1, free);

    char *clave1 = "perro", *valor1a, *valor1b;
    char *clave2 = "gato", *valor2a, *valor2b;

    /* Pide memoria para 4 valores */
    valor1a = malloc(10 * sizeof(char));
    valor1b = malloc(10 * sizeof(char));
    valor2a = malloc(10 * sizeof(char));
    valor2b = malloc(10 * sizeof(char));

    /* Inserta 2 valores y luego los reemplaza (debe liberar lo que reemplaza) */
    print_test("Prueba abb insertar clave1", abb_guardar(abb, clave1, valor1a));
    print_test("Prueba abb obtener clave1 es valor1a", abb_obtener(abb, clave1) == valor1a);
    print_test("Prueba abb obtener clave1 es valor1a", abb_obtener(abb, clave1) == valor1a);
    print_test("Prueba abb insertar clave2", abb_guardar(abb, clave2, valor2a));
    print_test("Prueba abb obtener clave2 es valor2a", abb_obtener(abb, clave2) == valor2a);
    print_test("Prueba abb obtener clave2 es valor2a", abb_obtener(abb, clave2) == valor2a);
    print_test("Prueba abb la cantidad de elementos es 2", abb_cantidad(abb) == 2);

    print_test("Prueba abb insertar clave1 con otro valor", abb_guardar(abb, clave1, valor1b));
    print_test("Prueba abb obtener clave1 es valor1b", abb_obtener(abb, clave1) == valor1b);
    print_test("Prueba abb obtener clave1 es valor1b", abb_obtener(abb, clave1) == valor1b);
    print_test("Prueba abb insertar clave2 con otro valor", abb_guardar(abb, clave2, valor2b));
    print_test("Prueba abb obtener clave2 es valor2b", abb_obtener(abb, clave2) == valor2b);
    print_test("Prueba abb obtener clave2 es valor2b", abb_obtener(abb, clave2) == valor2b);
    print_test("Prueba abb la cantidad de elementos es 2", abb_cantidad(abb) == 2);

    /* Se destruye el abb (se debe liberar lo que quedó dentro) */
    abb_destruir(abb);
    printf("FIN PRUEBA REEMPLAZA CON DESTRUIR\n");
}


bool prueba_borrar_visitar(const char* clave, void* valor, void* extra){
    pila_t* pila = extra;
    pila_apilar(pila, prueba_copiar_clave(clave));
    return true;
}

void prueba_borrar_aux(const char* clave, int* valor){
    abb_t* abb = pruebas_crear_arbol();
    print_test("Prueba abb crear arbol", abb);
    printf("clave: %s, valor: %i\n", clave, *valor);
    print_test("  Prueba abb pertenece clave", abb_pertenece(abb, clave));
    print_test("  Prueba abb borrar clave", (abb_borrar(abb, clave) == valor));
    print_test("  Prueba abb pertenece clave es false", !abb_pertenece(abb, clave));
    print_test("  Prueba abb borrar clave es NULL", abb_borrar(abb, clave) == NULL);


    // Pruebo que todas las otras claves siguen estando.
    bool otras_claves_ok = true;
    for(size_t i = 0; i < cant ; i++){
        if(strcmp(claves[i], clave) != 0){
            if(!abb_pertenece(abb, claves[i])){
                otras_claves_ok = false;
                break;
            }
        }
    }
    print_test("  Prueba abb las otras claves siguen estando", otras_claves_ok);

    // Pruebo que los valores que quedaron son los correctos.
    bool otros_valores_ok = true;
    for(size_t i = 0; i < cant ; i++){
        if(strcmp(claves[i], clave) != 0){
            if(abb_obtener(abb, claves[i]) != valores[i]){
                otros_valores_ok = false;
                break;
            }
        }
    }
    print_test("  Prueba abb los valores que quedaron son los correctos", otros_valores_ok);

    // Prueba in order
    pila_t* pila = pila_crear();

    abb_in_order(abb, prueba_borrar_visitar, pila);

    char* mayor = pila_desapilar(pila);
    bool in_order_ok = true;
    while(!pila_esta_vacia(pila) && in_order_ok){
        char* clave_desapilada = pila_desapilar(pila);
        if(strcmp(clave_desapilada, mayor) > 0){
            in_order_ok = false;
        }
        free(clave_desapilada);
    }
    free(mayor);
    print_test("  Prueba in order", in_order_ok);
    pila_destruir(pila);
    abb_destruir(abb);
}

void prueba_abb_borrar(){
    printf("\n");
    printf("INICIO PRUEBA BORRAR\n");
    for(size_t i = 0; i < cant; i++){
        prueba_borrar_aux(claves[i], valores[i]);
    }
    printf("FIN PRUEBA BORRAR\n");
}

void prueba_abb_borrar_in_order_aux(abb_t* abb, const char* clave, void* valor){

    printf("clave: %s, valor: %i\n", clave, *(int*)valor);
    print_test("  Prueba abb pertenece clave", abb_pertenece(abb, clave));
    print_test("  Prueba abb borrar clave es valor", abb_borrar(abb, clave) == valor);
    print_test("  Prueba abb pertenece clave es false", !abb_pertenece(abb, clave));
    print_test("  Prueba abb borrar clave es NULL", abb_borrar(abb, clave) == NULL);


    // Prueba in order
    pila_t* pila = pila_crear();
    abb_in_order(abb, prueba_borrar_visitar, pila);

    bool in_order_ok = true;

    while(!pila_esta_vacia(pila) && in_order_ok){
        char* desapilado_1 = pila_desapilar(pila);
        char* tope = pila_ver_tope(pila);
        if(tope && strcmp(tope, desapilado_1) > 0){
            in_order_ok = false;
        }
        free(desapilado_1);

    }
    print_test("  Prueba in order", in_order_ok);
    pila_destruir(pila);
}

void prueba_abb_borrar_in_order(){
    printf("\n");
    printf("INICIO PRUEBA BORRADO IN ORDER\n");

    abb_t* abb = pruebas_crear_arbol();
    print_test("Crear arbol", abb != NULL);

    for(size_t i = 0; i < cant; i++){
        prueba_abb_borrar_in_order_aux(abb, claves_in_order[i], valores_in_order[i]);
    }
    abb_destruir(abb);
    printf("FIN PRUEBA BORRADO IN ORDER\n");
}


void prueba_iterador_interno_in_order(){
    printf("\n");
    printf("INICIO PRUEBA ITERADOR INTERNO IN ORDER\n");
    abb_t* abb = pruebas_crear_arbol();
    print_test("Prueba crear arbol", abb);

    int* suma_pares = malloc(sizeof(int));
    *suma_pares = 0;
    int* suma_menores_a_15 = malloc(sizeof(int));
    *suma_menores_a_15 = 0;

    abb_in_order(abb, visitar_sumar_valores_pares, suma_pares);
    print_test("Prueba in order suma valores pares", *suma_pares == suma_valores_pares);

    abb_in_order(abb, visitar_sumar_valores_menores_a_15, suma_menores_a_15);
    print_test("Prueba in order suma mayores a 15", *suma_menores_a_15 == suma_valores_menores_a_15);

    free(suma_pares);
    free(suma_menores_a_15);
    abb_destruir(abb);
    printf("FIN PRUEBA INTERADOR INTERNO IN ORDER\n");
}

void prueba_iterar_abb_vacio()
{
    printf("\n");
    printf("INICIO PRUEBA ITERADOR EXTERNO IN ORDER - ARBOL VACIO\n");

    abb_t* abb = abb_crear(abb_comparar_clave_1, NULL);
    abb_iter_t* iter = abb_iter_in_crear(abb);
    print_test("Prueba abb iter crear iterador abb vacio", iter);
    print_test("Prueba abb iter esta al final", abb_iter_in_al_final(iter));
    print_test("Prueba abb iter avanzar es false", !abb_iter_in_avanzar(iter));
    print_test("Prueba abb iter ver actual es NULL", !abb_iter_in_ver_actual(iter));

    abb_iter_in_destruir(iter);
    abb_destruir(abb);
    printf("FIN PRUEBA ITERADOR EXTERNO IN ORDER - ARBOL VACIO\n");
}

void prueba_abb_iterarador_externo_in_order()
{
    printf("\n");
    printf("INICIO PRUEBA ITERADOR EXTERNO IN ORDER\n");

    abb_t* abb = pruebas_crear_arbol();
    print_test("Prueba crear arbol", abb);
    abb_iter_t* iter = abb_iter_in_crear(abb);
    print_test("Prueba crear iterador", iter);

    print_test("Iter ver actual es 2", strcmp(abb_iter_in_ver_actual(iter), "02") == 0);
    print_test("Iter avanzar es true", abb_iter_in_avanzar(iter));
    print_test("Iter ver actual es 5", strcmp(abb_iter_in_ver_actual(iter), "05") == 0);
    print_test("Iter avanzar es true", abb_iter_in_avanzar(iter));
    print_test("Iter ver actual es 7", strcmp(abb_iter_in_ver_actual(iter), "07") == 0);
    print_test("Iter avanzar es true", abb_iter_in_avanzar(iter));
    print_test("Iter ver actual es 9", strcmp(abb_iter_in_ver_actual(iter), "09") == 0);
    print_test("Iter avanzar es true", abb_iter_in_avanzar(iter));
    print_test("Iter ver actual es 12", strcmp(abb_iter_in_ver_actual(iter), "12") == 0);
    print_test("Iter avanzar es true", abb_iter_in_avanzar(iter));


    print_test("Iter ver actual es 13", strcmp(abb_iter_in_ver_actual(iter), "13") == 0);
    print_test("Iter avanzar es true", abb_iter_in_avanzar(iter));
    print_test("Iter ver actual es 15", strcmp(abb_iter_in_ver_actual(iter), "15") == 0);
    print_test("Iter avanzar es true", abb_iter_in_avanzar(iter));
    print_test("Iter ver actual es 16", strcmp(abb_iter_in_ver_actual(iter), "16") == 0);
    print_test("Iter avanzar es true", abb_iter_in_avanzar(iter));
    print_test("Iter ver actual es 17", strcmp(abb_iter_in_ver_actual(iter), "17") == 0);
    print_test("Iter avanzar es true", abb_iter_in_avanzar(iter));
    print_test("Iter ver actual es 18", strcmp(abb_iter_in_ver_actual(iter), "18") == 0);
    print_test("Iter avanzar es true", abb_iter_in_avanzar(iter));
    print_test("Iter ver actual es 19", strcmp(abb_iter_in_ver_actual(iter), "19") == 0);
    print_test("Iter avanzar es true", abb_iter_in_avanzar(iter));
    print_test("Iter ver actual es 20", strcmp(abb_iter_in_ver_actual(iter), "20") == 0);
    print_test("Iter avanzar es true", abb_iter_in_avanzar(iter));
    print_test("Iter ver actual es NULL", abb_iter_in_ver_actual(iter) == NULL);
    print_test("Iter avanzar es false", abb_iter_in_avanzar(iter) == false);

    abb_iter_in_destruir(iter);
    abb_destruir(abb);
    printf("FIN PRUEBA ITERADOR EXTERNO IN ORDER\n");
}

void prueba_abb_volumen(size_t largo, bool debug)
{
    printf("\n");
    printf("INICIO PRUEBA VOLUMEN\n");
    abb_t* abb = abb_crear(abb_comparar_clave_1, NULL);

    const size_t largo_clave = 10;
    char* *claves_volumen = malloc((largo + 1) * sizeof(char*));

    unsigned* valores_volumen[largo];

    /* Inserta 'largo' parejas en el abb */
    bool ok = true;
    for (unsigned i = 0; i < largo; i++) {
        char* clave_aleatoria = crear_clave_aleatoria(largo_clave);
        if(!clave_aleatoria){
            ok = false;
            break;
        }
        claves_volumen[i] = clave_aleatoria;

        valores_volumen[i] = malloc(sizeof(int));
        *valores_volumen[i] = i;

        ok = abb_guardar(abb, clave_aleatoria, valores_volumen[i]);
        if (!ok) break;
    }

    if (debug) print_test("Prueba abb almacenar muchos elementos", ok);
    if (debug) print_test("Prueba abb la cantidad de elementos es correcta", abb_cantidad(abb) == largo);

    /* Verifica que devuelva los valores correctos */
    for (size_t i = 0; i < largo; i++) {
        ok = abb_pertenece(abb, claves_volumen[i]);
        if (!ok) break;
        ok = abb_obtener(abb, claves_volumen[i]) == valores_volumen[i];
        if (!ok) break;
    }

    if (debug) print_test("Prueba abb pertenece y obtener muchos elementos", ok);
    if (debug) print_test("Prueba abb la cantidad de elementos es correcta", abb_cantidad(abb) == largo);


    /* Verifica que borre y devuelva los valores correctos */
    for (size_t i = 0; i < largo; i++) {
        ok = abb_borrar(abb, claves_volumen[i]) == valores_volumen[i];
        if (!ok) break;
    }

    if (debug) print_test("Prueba abb borrar muchos elementos", ok);
    if (debug) print_test("Prueba abb la cantidad de elementos es 0", abb_cantidad(abb) == 0);

    /* Destruye el abb y crea uno nuevo que sí libera */
    abb_destruir(abb);
    abb = abb_crear(abb_comparar_clave_1, free);

    /* Inserta 'largo' parejas en el abb */
    ok = true;
    for (size_t i = 0; i < largo; i++) {
        ok = abb_guardar(abb, claves_volumen[i], valores_volumen[i]);
        if (!ok) break;
    }

    for(size_t i = 0; i < largo; i++){
        free(claves_volumen[i]);
    }
    free(claves_volumen);


    /* Destruye el abb - debería liberar los enteros */
    abb_destruir(abb);
    printf("FIN PRUEBA VOLUMEN\n");

}



void prueba_abb_iterar_volumen(size_t largo)
{
    printf("\n");
    printf("INICIO PRUEBA ITERAR VOLUMEN\n");
    abb_t*abb = abb_crear(abb_comparar_clave_1, NULL);

    const size_t largo_clave = 10;

    char* *claves_volumen = malloc((largo + 1) * sizeof(char*));
    size_t valores_volumen[largo];

    /* Inserta 'largo' parejas en el abb */
    bool ok = true;
    for (unsigned i = 0; i < largo; i++) {
        char* clave_aleatoria = crear_clave_aleatoria(largo_clave);
        if(!clave_aleatoria){
            ok = false;
            break;
        }
        claves_volumen[i] = clave_aleatoria;

        valores_volumen[i] = i;
        ok = abb_guardar(abb, claves_volumen[i], &valores_volumen[i]);
        if (!ok) break;
    }

    // Prueba de iteración sobre las claves almacenadas.
    abb_iter_t* iter = abb_iter_in_crear(abb);
    print_test("Prueba abb iterador esta al final, es false", !abb_iter_in_al_final(iter));

    ok = true;
    unsigned i;
    const char *clave;
    size_t *valor;

    for (i = 0; i < largo; i++) {
        if ( abb_iter_in_al_final(iter) ) {
            ok = false;
            break;
        }
        clave = abb_iter_in_ver_actual(iter);
        if ( clave == NULL ) {
            ok = false;
            break;
        }
        valor = abb_obtener(abb, clave);
        if ( valor == NULL ) {
            ok = false;
            break;
        }
        *valor = largo;
        abb_iter_in_avanzar(iter);
    }
    print_test("Prueba abb iteración en volumen", ok);
    print_test("Prueba abb iteración en volumen, recorrio todo el largo", i == largo);
    print_test("Prueba abb iterador esta al final, es true", abb_iter_in_al_final(iter));

    ok = true;
    for (i = 0; i < largo; i++) {
        if ( valores_volumen[i] != largo ) {
            ok = false;
            break;
        }
    }
    print_test("Prueba abb iteración en volumen, se cambiaron todo los elementos", ok);

    for(i = 0; i < largo; i++){
        free(claves_volumen[i]);
    }
    free(claves_volumen);
    abb_iter_in_destruir(iter);
    abb_destruir(abb);
    printf("FIN PRUEBA ITERAR VOLUMEN\n");
}



void pruebas_abb_alumno(){
    prueba_crear_abb_vacio();
    prueba_abb_clave_vacia();
    prueba_abb_valor_null();
    prueba_abb_insertar();
    prueba_abb_reemplazar();
    prueba_abb_reemplazar_con_destruir();
    prueba_abb_borrar();
    prueba_abb_borrar_in_order();
    prueba_iterador_interno_in_order();
    prueba_iterar_abb_vacio();
    prueba_abb_iterarador_externo_in_order();
    prueba_abb_volumen(500, true);
    prueba_abb_iterar_volumen(500);
}