CC = gcc
CFLAGS = -g -std=c99 -Wall -Wconversion -Wno-sign-conversion -Werror -pedantic
VALFLAGS = --leak-check=full --track-origins=yes --show-reachable=yes
EXEC = pruebas

%.o: %.c %.h
	$(CC) $(CFLAGS) -c $<

ABB_OBJECTS = abb.o abb_nodo.o main.o pila.o pruebas_abb_alumno.o testing.o
build: $(ABB_OBJECTS) 
	$(CC) $(CFLAGS) -o $(EXEC) $(ABB_OBJECTS)
	$(MAKE) copy
	
run: build
	./$(EXEC)
	$(MAKE) clean
	
val: build
	valgrind $(VALFLAGS) ./$(EXEC)
	$(MAKE) clean

copy:
	cp $(ABB) ./pruebas_catedra/

clean:
	rm -f *.o $(EXEC) $(ZIPNAME)

ABB = abb.c abb.h
ABB += abb_nodo.c abb_nodo.h
ABB += pila.c pila.h

ENTREGA = $(ABB)
ENTREGA += pruebas_abb_alumno.c
ENTREGA += Makefile

ZIPNAME = abb_entrega.zip

zip:
	zip $(ZIPNAME) $(ENTREGA)




