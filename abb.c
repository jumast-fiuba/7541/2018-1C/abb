#include <stdlib.h>
#include "abb.h"
#include "abb_nodo.h"
#include "pila.h"

struct abb{
    abb_nodo_t* _raiz;
    abb_comparar_clave_t _comparar_clave;
    abb_destruir_dato_t  _destruir_dato;
    size_t _cant;
};

struct abb_iter{
    pila_t* _visitados;
};

/* ******************************************************************
 *            ABB - Declaración de funciones auxiliares
 * *****************************************************************/
static void destruir_dato_dummy(void *dato);

static abb_nodo_t* buscar_nodo_r(abb_nodo_t *nodo, const char *clave, abb_comparar_clave_t cmp);
static void abb_destruir_r(abb_nodo_t* nodo, abb_destruir_dato_t destruir_dato);
static abb_nodo_t* abb_guardar_r(abb_t* arbol, abb_nodo_t* padre, abb_nodo_t* nodo, const char* clave, void* dato);

static void abb_transplantar(abb_t* abb, abb_nodo_t* nodo_a_borrar, abb_nodo_t* reemplazo);
static void abb_borrar_nodo_con_dos_hijos(abb_t* arbol, abb_nodo_t* nodo_a_borrar);

/* ******************************************************************
 *            ABB ITER - Declaración de funciones auxiliares
 * *****************************************************************/
static bool abb_in_order_r(abb_nodo_t* nodo, bool visitar(const char *, void *, void *), void *extra);
static void apilar_todos_los_hijos_izq(pila_t *pila, abb_nodo_t *nodo);

/* ******************************************************************
 *                         ABB - PRIMITIVAS
 * *****************************************************************/
abb_t* abb_crear(abb_comparar_clave_t cmp, abb_destruir_dato_t destruir_dato){
    abb_t* abb = malloc(sizeof(abb_t));
    if(abb == NULL)
        return NULL;

    abb->_raiz = NULL;
    abb->_comparar_clave = cmp;
    abb->_destruir_dato = destruir_dato != NULL ? destruir_dato : destruir_dato_dummy;
    abb->_cant = 0;

    return abb;
}

void abb_destruir(abb_t* arbol){
    abb_destruir_r(arbol->_raiz, arbol->_destruir_dato);
    free(arbol);
}
void abb_destruir_r(abb_nodo_t* nodo, abb_destruir_dato_t destruir_dato){
    if(nodo == NULL)
        return;
    abb_destruir_r(nodo->_hizo_izq, destruir_dato);
    abb_destruir_r(nodo->_hijo_der, destruir_dato);
    abb_nodo_destruir(nodo, destruir_dato);
}

bool abb_guardar(abb_t* arbol, const char *clave, void* dato) {
    return abb_guardar_r(arbol, NULL, arbol->_raiz, clave, dato) != NULL;
}
abb_nodo_t* abb_guardar_r(abb_t* arbol, abb_nodo_t* padre, abb_nodo_t* nodo, const char* clave, void* dato){
    if (nodo == NULL) {
        abb_nodo_t* nuevo_nodo = abb_nodo_crear(clave, dato, padre, NULL, NULL);
        if(nuevo_nodo == NULL)
            return NULL;

        if(arbol->_raiz == NULL)
            arbol->_raiz = nuevo_nodo;

        arbol->_cant++;
        return nuevo_nodo;
    }

    int comparacion = arbol->_comparar_clave(clave, abb_nodo_obtener_clave(nodo));
    if(comparacion == 0){
        abb_nodo_reemplazar_valor(nodo, dato, arbol->_destruir_dato);
        return nodo;
    }
    if(comparacion < 0)
        nodo->_hizo_izq = abb_guardar_r(arbol, nodo, nodo->_hizo_izq, clave, dato);
    else
        nodo->_hijo_der = abb_guardar_r(arbol, nodo, nodo->_hijo_der, clave, dato);
    return nodo;
}

void* abb_borrar(abb_t* arbol, const char* clave){
    abb_nodo_t* nodo_a_borrar = buscar_nodo_r(arbol->_raiz, clave, arbol->_comparar_clave);
    if(nodo_a_borrar == NULL)
        return NULL;

    void* valor = abb_nodo_obtener_valor(nodo_a_borrar);
    if(!abb_nodo_tiene_hijo_izquierdo(nodo_a_borrar)){
        abb_nodo_t* reemplazo = nodo_a_borrar->_hijo_der;
        // borrar nodo sin hijo izquierdo
        abb_transplantar(arbol, nodo_a_borrar, reemplazo);
    }
    else if(!abb_nodo_tiene_hijo_derecho(nodo_a_borrar)){
        abb_nodo_t* reemplazo = nodo_a_borrar->_hizo_izq;
        // borrar nodo sin hijo derecho
        abb_transplantar(arbol, nodo_a_borrar, reemplazo);
    }
    else{
        abb_borrar_nodo_con_dos_hijos(arbol, nodo_a_borrar);
    }
    arbol->_cant--;
    abb_nodo_destruir(nodo_a_borrar, arbol->_destruir_dato);
    return valor;
}

void* abb_obtener(const abb_t* arbol, const char* clave){
    abb_nodo_t* buscado = buscar_nodo_r(arbol->_raiz, clave, arbol->_comparar_clave);
    return buscado == NULL ? NULL : abb_nodo_obtener_valor(buscado);
}

bool abb_pertenece(const abb_t* arbol, const char* clave){
    abb_nodo_t* buscado = buscar_nodo_r(arbol->_raiz, clave, arbol->_comparar_clave);
    return buscado == NULL ? false : true;
}

size_t abb_cantidad(abb_t* arbol){
    return arbol->_cant;
}

/* ******************************************************************
 *                ABB ITERADOR INTERNO - PRIMITIVAS
 * *****************************************************************/
void abb_in_order(abb_t *arbol, bool visitar(const char *, void *, void *), void *extra){
    abb_in_order_r(arbol->_raiz, visitar, extra);
}
bool  abb_in_order_r(abb_nodo_t* nodo, bool visitar(const char *, void *, void *), void *extra){
    if(nodo == NULL)
        return false;

    if (nodo->_hizo_izq) {
        if(!abb_in_order_r(nodo->_hizo_izq, visitar, extra)){
            return false;
        };
    }

    if(!visitar(abb_nodo_obtener_clave(nodo), abb_nodo_obtener_valor(nodo), extra)){
        return false;
    }
    else if(nodo->_hijo_der) {
        if(!abb_in_order_r(nodo->_hijo_der, visitar, extra)){
            return false;
        }
    }
    return true;
}
/* ******************************************************************
 *                ABB ITERADOR EXTERNO - PRIMITIVAS
 * *****************************************************************/
abb_iter_t* abb_iter_in_crear(const abb_t* abb){
    abb_iter_t* iter = malloc(sizeof(abb_iter_t));
    if(iter == NULL)
        return NULL;

    pila_t* pila = pila_crear();
    if(pila == NULL){
        free(iter);
        return NULL;
    }
    iter->_visitados = pila;

    if(abb->_raiz != NULL){
        pila_apilar(iter->_visitados, abb->_raiz);
    }

    apilar_todos_los_hijos_izq(iter->_visitados, abb->_raiz);
    return iter;
}


void abb_iter_in_destruir(abb_iter_t* iter){
    pila_destruir(iter->_visitados);
    free(iter);
}

bool abb_iter_in_avanzar(abb_iter_t* iter){

    if(abb_iter_in_al_final(iter)){
        return false;
    }

    abb_nodo_t* desapilado = pila_desapilar(iter->_visitados);
    if (desapilado->_hijo_der != NULL){
        pila_apilar(iter->_visitados, desapilado->_hijo_der);
        apilar_todos_los_hijos_izq(iter->_visitados, desapilado->_hijo_der);
    }
    return true;
}

const char* abb_iter_in_ver_actual(const abb_iter_t* iter){
    if(abb_iter_in_al_final(iter)){
        return NULL;
    }
    return abb_nodo_obtener_clave(pila_ver_tope(iter->_visitados));
}

bool abb_iter_in_al_final(const abb_iter_t* iter){
    return pila_esta_vacia(iter->_visitados);
}

/* ******************************************************************
 *                    ABB - Funciones auxiliares
 * *****************************************************************/
static bool es_raiz(abb_nodo_t* nodo, abb_t* arbol);
static abb_nodo_t* obtener_minimo(abb_nodo_t *nodo);

static void destruir_dato_dummy(void *dato){
    // hacer nada
    ;
}

static abb_nodo_t* buscar_nodo_r(abb_nodo_t *nodo, const char *clave, abb_comparar_clave_t cmp){
    if(nodo == NULL){
        return NULL;
    }

    int comparacion = cmp(clave, abb_nodo_obtener_clave(nodo));
    if(comparacion == 0){
        return nodo;
    }

    if(comparacion < 0){
        return buscar_nodo_r(nodo->_hizo_izq, clave, cmp);
    }
    else{
        return buscar_nodo_r(nodo->_hijo_der, clave, cmp);
    }
}

static void abb_transplantar(abb_t* abb, abb_nodo_t* nodo_a_borrar, abb_nodo_t* reemplazo){
    if(es_raiz(nodo_a_borrar, abb)){
        abb->_raiz = reemplazo;
    }
    else if(abb_nodo_es_hijo_izquierdo(nodo_a_borrar)){
        nodo_a_borrar->_padre->_hizo_izq = reemplazo;
    }
    else{
        nodo_a_borrar->_padre->_hijo_der = reemplazo;
    }
    if(reemplazo){
        reemplazo->_padre = nodo_a_borrar->_padre;
    }
}

static void abb_borrar_nodo_con_dos_hijos(abb_t* arbol, abb_nodo_t* nodo_a_borrar){
    abb_nodo_t* sucesor = obtener_minimo(nodo_a_borrar->_hijo_der);
    abb_nodo_t* reemplazo = sucesor;

    if(reemplazo->_padre != nodo_a_borrar){
        abb_transplantar(arbol, reemplazo, reemplazo->_hijo_der);
        reemplazo->_hijo_der = nodo_a_borrar->_hijo_der;
        reemplazo->_hijo_der->_padre = reemplazo;
    }
    abb_transplantar(arbol, nodo_a_borrar, reemplazo);
    reemplazo->_hizo_izq = nodo_a_borrar->_hizo_izq;
    reemplazo->_hizo_izq->_padre = reemplazo;
}
static abb_nodo_t* obtener_minimo(abb_nodo_t *nodo){
    while(nodo->_hizo_izq != NULL){
        nodo = nodo->_hizo_izq;
    }
    return nodo;
}

static bool es_raiz(abb_nodo_t* nodo, abb_t* arbol){
    return nodo == arbol->_raiz;
}

/* ******************************************************************
 *                  ABB ITER - Funciones auxiliares
 * *****************************************************************/
static void apilar_todos_los_hijos_izq(pila_t *pila, abb_nodo_t *nodo){
    if(nodo == NULL)
        return;

    while(nodo->_hizo_izq){
        pila_apilar(pila, nodo->_hizo_izq);
        nodo = nodo->_hizo_izq;
    }
}