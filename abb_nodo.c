#include <stdlib.h>
#include <string.h>
#include "abb_nodo.h"

static char* copiar_clave(const char *clave){
    char* copia = malloc(1 + strlen(clave));
    if(!copia){
        return NULL;
    }
    strcpy(copia, clave);
    return copia;
}

abb_nodo_t * abb_nodo_crear(const char* clave, void* valor, abb_nodo_t * padre, abb_nodo_t * hijo_izq, abb_nodo_t * hijo_der){
    abb_nodo_t * abb_nodo = malloc(sizeof(abb_nodo_t));
    if(abb_nodo == NULL)
        return NULL;

    char* copia_clave = copiar_clave(clave);
    if(copia_clave == NULL){
        free(abb_nodo);
        return NULL;
    }

    abb_nodo->_clave = copia_clave;
    abb_nodo->_valor = valor;
    abb_nodo->_padre = padre;
    abb_nodo->_hizo_izq = hijo_izq;
    abb_nodo->_hijo_der = hijo_der;

    return abb_nodo;
}

void abb_nodo_destruir(abb_nodo_t * abb_nodo, abb_destruir_dato_t destruir_dato){
    free(abb_nodo->_clave);
    if(destruir_dato){
        destruir_dato(abb_nodo->_valor);
    }
    free(abb_nodo);
}

const char* abb_nodo_obtener_clave(abb_nodo_t* abb_nodo){
    return abb_nodo->_clave;
}

void* abb_nodo_obtener_valor(abb_nodo_t* abb_nodo){
    return abb_nodo->_valor;
}

void abb_nodo_reemplazar_valor(abb_nodo_t* abb_nodo, void* valor_nuevo, abb_destruir_dato_t destruir_valor){
    if(destruir_valor){
        destruir_valor(abb_nodo->_valor);
    }
    abb_nodo->_valor = valor_nuevo;
}

bool abb_nodo_es_hijo_izquierdo(abb_nodo_t* hijo){
    return hijo->_padre->_hizo_izq == hijo;
}

bool abb_nodo_tiene_hijo_izquierdo(abb_nodo_t* nodo){
    return nodo->_hizo_izq != NULL;
}

bool abb_nodo_tiene_hijo_derecho(abb_nodo_t* nodo){
    return nodo->_hijo_der != NULL;
}